#include <stdio.h> // 202403251.021240: dwrr   a windowing system just for fun.
#include <string.h>  // and maybe it might be useful one day idk
#include <stdlib.h>
#include <iso646.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>  
#include <stdlib.h> 
#include <string.h> 
#include <iso646.h> 
#include <unistd.h>
#include <fcntl.h>  
#include <termios.h>
#include <time.h>
#include <stdbool.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdint.h>
#include <signal.h>    
#include <stdnoreturn.h> 

typedef ssize_t nat;

static struct winsize window = {0};
static struct termios terminal = {0};
extern char** environ;

struct location {
	nat x;
	nat y;
};

struct window {
	struct location size;
	struct location position;
	const char* title;
	int background_color;
	int border_fg_color;
	int border_bg_color;
	bool closing;
	bool resizing;
	bool repositioning;
	bool _padding_;
	nat reposition_from;
};

static void clear_screen(void) {
	printf("\033[H\033[2J");
}

static void position_at(struct location position) {
	printf("\033[%lu;%luH", position.y + 1, position.x + 1);
}

static void reset_color(void) { printf("\033[0m"); }

static void set_color(int foreground, int background) {
	printf("\033[48;5;%dm\033[38;5;%dm", background, foreground);
}

static void draw_window(struct window this) {

	position_at(this.position);
	set_color(1, this.border_bg_color);
	printf("X");
	set_color(this.border_fg_color, this.border_bg_color);
	for (nat i = 0; i < this.size.x - 2; i++) printf("=");
	printf("+");
	reset_color();
	putchar(10);
	this.position.y++;

	for (nat i = 0; i < this.size.y - 2; i++) {
		position_at(this.position);
		set_color(this.border_fg_color, this.border_bg_color);
		printf("|");
		reset_color();


		if (i == 0) {
			set_color(0, this.background_color);
			printf("%-5lu", this.position.x);
			for (nat k = 0; k < this.size.x - 2 - 5; k++) printf(".");
			reset_color();

		} else if (i == 1) {
			set_color(0, this.background_color);
			printf("%-5lu", this.position.y);
			for (nat k = 0; k < this.size.x - 2 - 5; k++) printf(".");
			reset_color();

		} else if (i == 2) {
			set_color(0, this.background_color);
			printf("%-5lu", this.size.x);
			for (nat k = 0; k < this.size.x - 2 - 5; k++) printf(".");
			reset_color();

		} else if (i == 3) {
			set_color(0, this.background_color);
			printf("%-5lu", this.size.y);
			for (nat k = 0; k < this.size.x - 2 - 5; k++) printf(".");
			reset_color();
			
		} else if (i == 4) {
			set_color(0, this.background_color);
			printf("%c%c%c", 
				this.resizing ? 'S' : ' ',
				this.repositioning ? 'P' : ' ',
				this.closing ? 'X' : ' '
			);
			for (nat k = 0; k < this.size.x - 2 - 3; k++) printf(".");
			reset_color();

		} else if (i == 5) {
			set_color(0, this.background_color);
			printf("%10s", this.title);
			for (nat k = 0; k < this.size.x - 2 - 10; k++) printf(".");
			reset_color();
			

		} else {
			set_color(0, this.background_color);
			for (nat k = 0; k < this.size.x - 2; k++) printf(".");
			reset_color();
		}

		set_color(this.border_fg_color, this.border_bg_color);
		printf("|");
		reset_color();
		putchar(10);
		this.position.y++;
	}
	position_at(this.position);
	set_color(this.border_fg_color, this.border_bg_color);
	printf("+");
	for (nat i = 0; i < this.size.x - 2; i++) printf("-");
	printf("*");
	reset_color();
	putchar(10);
}

static void window_resized(int _) {if(_){} ioctl(0, TIOCGWINSZ, &window); }
static noreturn void interrupted(int _) {if(_){} 
	write(1, "\033[?1000l\033[?25h\033[?1049l", 14 + 8);
	tcsetattr(0, TCSAFLUSH, &terminal);
	exit(0); 
}

int main(void) {
	struct sigaction action = {.sa_handler = window_resized}; 
	sigaction(SIGWINCH, &action, NULL);
	struct sigaction action2 = {.sa_handler = interrupted}; 
	sigaction(SIGINT, &action2, NULL);

	tcgetattr(0, &terminal);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_cc[VMIN] = 1; 
	terminal_copy.c_cc[VTIME] = 0;  //vmin=1,vtime=0
	terminal_copy.c_iflag &= ~((size_t) IXON);
	terminal_copy.c_lflag &= ~((size_t) ECHO | ICANON);
	tcsetattr(0, TCSAFLUSH, &terminal_copy);
	write(1, "\033[?1049h\033[?25l\033[?1000h", 14 + 8);

	struct window asht = {
		.title = "main",
		.size.x = 16,
		.size.y = 9,
		.position.x = 10,
		.position.y = 10,
		.background_color = 255,
		.border_fg_color = 243,
		.border_bg_color = 0,
	};

	struct window asht2 = {
		.title = "new",
		.size.x = 9,
		.size.y = 9,
		.position.x = 20,
		.position.y = 20,
		.background_color = 245,
		.border_fg_color = 243,
		.border_bg_color = 0,
	};


	nat box_count = 0;
	struct window boxes[4096] = {0};

	boxes[box_count++] = asht;
	boxes[box_count++] = asht2;

//	nat input_count = 0;
//	int input[4096] = {0};

	nat active = 0;

loop:;	
	ioctl(0, TIOCGWINSZ, &window);
	clear_screen();
	for (nat i = 0; i < box_count; i++) if (i != active) draw_window(boxes[i]);
	draw_window(boxes[active]);

/*	printf("input: { ");
	for (nat i = 0; i < input_count; i++) {
		if (i % 12 == 0) puts("");
		printf("%d(%c) ", input[i], input[i] >= 32 ? input[i] : '_');
	}
	printf("}\n");
	fflush(stdout);
*/
	char c = 0;
	read(0, &c, 1);

	if (c == 27) { 
		//goto add_to_input;
		char b = 0, cx = 0, cy = 0;
		read(0, &c, 1); // [
		read(0, &c, 1); // M
		c = 0;
		read(0, &b, 1); // Cb button pressed
		read(0, &cx, 1); 
		read(0, &cy, 1); 
		const nat x = (nat) (uint64_t) (((uint8_t) cx) - 33); 
		const nat y = (nat) (uint64_t) (((uint8_t) cy) - 33);
		//printf("user %s the coords <x=%d,y=%d>\n", b == ' ' ? "pressed" : "released", x, y);
		
		for (nat i = 0; i < box_count; i++) {
			if (b == ' ') {
				if (
					x > boxes[i].position.x and 
					x < boxes[i].position.x + boxes[i].size.x and 
					y == boxes[i].position.y
				) { boxes[i].repositioning = true; active = i; boxes[i].reposition_from = x - boxes[i].position.x; }

				else if (
					x == boxes[i].position.x and 
					y == boxes[i].position.y
				) { boxes[i].closing = true; active = i; }

				else if (
					x == boxes[i].position.x + boxes[i].size.x - 1 and
					y == boxes[i].position.y + boxes[i].size.y - 1
				) { boxes[i].resizing = true; active = i; }

			} else {
				if (boxes[i].resizing) {
					boxes[i].size.x = x - boxes[i].position.x;
					boxes[i].size.y = y - boxes[i].position.y;

					if (boxes[i].size.x < 5) boxes[i].size.x = 5;
					if (boxes[i].size.y < 5) boxes[i].size.y = 5;
					
					boxes[i].resizing = false;
					break;

				} else if (boxes[i].repositioning) {
					boxes[i].position.x = x - boxes[i].reposition_from;
					boxes[i].position.y = y;
					if (boxes[i].position.x < 0) boxes[i].position.x = 0;
					if (boxes[i].position.y < 0) boxes[i].position.y = 0;
					if (boxes[i].position.x + boxes[i].size.x > window.ws_col - 3) 
						boxes[i].position.x = window.ws_col - 3 - boxes[i].size.x;
					if (boxes[i].position.y + boxes[i].size.y > window.ws_row - 3) 
						boxes[i].position.y = window.ws_row - 3 - boxes[i].size.y;
					boxes[i].repositioning = false;
					break;

				} else if (boxes[i].closing) {
					boxes[i].closing = false;
					if (x == boxes[i].position.x and y == boxes[i].position.y) {
						box_count--;
						if (active == box_count) active--;
						memmove(boxes + i, boxes + i + 1, (size_t) (box_count - i) * sizeof(struct window));
						break;
					}
				}
			}
		}
	}
	else if (c == 'q') goto done;
	else if (c == 't') {
		boxes[active].background_color = not boxes[active].background_color ? 255 : 0;
	}

	else if (c == 'n') {
		boxes[box_count++] = asht;
		active = box_count - 1;
	}
//add_to_input:
	//input[input_count++] = (int) c;


	if (box_count) goto loop;

done:	write(1, "\033[?1000l\033[?25h\033[?1049l", 14 + 8);
	tcsetattr(0, TCSAFLUSH, &terminal);
}
































//position_at((struct location) {0, 0});
	//set_color(0, 45);
	//puts("hello there!");
	//reset_color();


/*



	if (argc < 2) goto new;
	strlcpy(filename, argv[1], sizeof filename);
	int df = open(filename, O_RDONLY | O_DIRECTORY);
	if (df >= 0) { close(df); errno = EISDIR; goto read_error; }
	int file = open(filename, O_RDONLY);
	if (file < 0) { read_error: perror("load: read open file"); exit(1); }
	struct stat s; fstat(file, &s);
	count = (nat) s.st_size;
	text = malloc(count);
	read(file, text, count);
	close(file);



*/









